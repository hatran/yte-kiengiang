var mook = {
    sobn: function () {
        let raw = `
        Bệnh viện huyện An Minh
        Bệnh viện huyện An Biên
        Bệnh viện huyện Vinh Thuận
        Bệnh viện huyện Giồng Riềng
        Bệnh viện huyện Gò Quao
        Bệnh viện huyện Châu Thành
        Bệnh viện huyện Tân Hiệp`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `
        Bệnh viện huyện An Minh
        Bệnh viện huyện An Biên
        Bệnh viện huyện Vinh Thuận
        Bệnh viện huyện Giồng Riềng
        Bệnh viện huyện Gò Quao
        Bệnh viện huyện Châu Thành
        Bệnh viện huyện Tân Hiệp`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `TP. Rạch Giá‎
                H. An Biên
                H. Châu Thành
                H. Giang Thành‎
                H. Giồng Riềng‎ 
                H. Gò Quao‎ 
                TP. Hà Tiên‎ 
                H. Hòn Đất
                H. Phú Quốc‎`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `
        Bệnh viện huyện An Minh
        Bệnh viện huyện An Biên
        Bệnh viện huyện Vinh Thuận
        Bệnh viện huyện Giồng Riềng
        Bệnh viện huyện Gò Quao
        Bệnh viện huyện Châu Thành
        Bệnh viện huyện Tân Hiệp`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `
        Bệnh viện huyện An Minh
        Bệnh viện huyện An Biên
        Bệnh viện huyện Vinh Thuận
        Bệnh viện huyện Giồng Riềng
        Bệnh viện huyện Gò Quao
        Bệnh viện huyện Châu Thành
        Bệnh viện huyện Tân Hiệp`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}

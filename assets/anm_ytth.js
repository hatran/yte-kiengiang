var mook = {
    sobn: function() {
        let raw = `Hệ thống thao trường mạng
        Hệ thống cyber case
        Hệ thống cyber gym`;
        let items = raw.split(/\n/);
        items = items.map(function(e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function() {
        let raw = `Hệ thống giám sát mạng
        Hệ thống honeypot
        Hệ thống phản hồi sự cố
        Hệ thống rà - quét lỗ hổng mạng
        Hệ thống quản lý truy cập mạng
        Hệ thống truy cập đặc quyền`;
        let items = raw.split(/\n/);
        items = items.map(function(e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function() {
        let raw = `Hệ thống tấn công DDOS
        Hệ thống phát tán malware
        Hệ thống trích xuất thông tin cao cấp
        Hệ thống quét lỗ hổng quân sự
        Hệ thống đánh cắp dữ liệu
        Hệ thống tấn công phising
        Hệ thống khai thác lỗ hổng web`;
        let items = raw.split(/\n/);
        items = items.map(function(e) {
            return e.trim();
        });
        return items;
    },
    cdv: function() {
        let raw = `Hệ thống webint
        Hệ thống OSINT
        Hệ thống thu chặn tín hiệu ĐTDD
        Hệ thống thu chặn tín hiệu wifi
        Hệ thống thu chặn tín hiệu vệ tinh
        Hệ thống trinh sát lỗ hổng mạng`;
        let items = raw.split(/\n/);
        items = items.map(function(e) {
            return e.trim();
        });
        return items;
    },

    http: function() {
        let raw = `1- BV Tuyến Tỉnh
        Bệnh viện đa khoa tỉnh
Bệnh viện Phụ sản
Bệnh viện Nhi
Bệnh viện Tâm thần
Bệnh viện Phổi
Bệnh viện huyện Châu Thành
Bệnh viện Mắt
Bệnh viện Nội tiết
Bệnh viện PHCN
Bệnh viện thị xã Hà Tiên
Bệnh viện ĐKKV Ngọc Lặc
Bệnh viện ĐKKV Tĩnh Gia
Bệnh viện Ung Bướu
BVĐKKV Hà Trung
BVĐK Quan Hoá
2 - BV Tuyến Huyện
BVĐK Thành phố
BVĐK Sầm Sơn
BVĐK Bỉm Sơn
BVĐK Thọ Xuân
BVĐK Đông Sơn
BVĐK Nông Cống
BVĐK Triệu Sơn
BVĐK Quảng Xương
BVĐK Nga Sơn
BVĐK Yên Định
BVĐK Thiệu Hoá
BVĐK Hoằng Hoá
BVĐK Hậu Lộc
BVĐK Vĩnh Lộc
BVĐK Thạch Thành
BVĐK Cẩm Thuỷ
BVĐK Lang Chánh
BVĐK Như Xuân
BVĐK Như Thanh
BVĐK Thường Xuân
BVĐK Bá Thước
BVĐK Quan Hoá
BVĐK Quan Sơn
BVĐK Mường Lát
3 - Trung tâm y tế
Trung tâm y tế huyện Hậu Lộc
Trung tâm y tế huyện Quan Sơn
Trung tâm y tế huyện Đông Sơn
Trung tâm y tế huyện Hà Trung
Trung tâm y tế huyện Hoằng Hóa
Trung tâm y tế huyện Cẩm Thủy
Trung tâm y tế huyện Như Xuân
Trung tâm y tế huyện Như Thanh
Trung tâm y tế huyện Thọ Xuân
Trung tâm y tế huyện Lang Chánh
Trung tâm y tế huyện Nga Sơn
Trung tâm y tế huyện Ngọc Lặc
Trung tâm y tế huyện Nông Cống
Trung tâm y tế huyện Quan Hóa
Trung tâm y tế huyện Quảng Xương
Trung tâm y tế huyện Thạch Thanh
Trung tâm y tế Thành phố Thanh Hóa
Trung tâm y tế thị xã Bỉm Sơn
Trung tâm y tế thị xã Sầm Sơn
Trung tâm y tế huyện Thiệu Hóa
Trung tâm y tế huyện Thường Xuân
Trung tâm y tế huyện Tĩnh Gia
Trung tâm y tế huyện Triệu Sơn
Trung tâm y tế huyện Bá Thước
Trung tâm y tế huyện Vĩnh Lộc
Trung tâm y tế huyện Yên Định
Trung tâm y tế huyện Mường Lát
 `;
        let items = raw.split(/\n/);
        items = items.map(function(e) {
            return e.trim();
        });
        return items;
    },
    backlist: function() {
        return mook.http();
    }
}

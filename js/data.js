var data = [
 {
   "STT": 2,
   "Name": "Bệnh viện đa khoa tỉnh Kiên Giang",
   "address": "05, Phạm Ngọc Thạch, Hiệp Thành, TP.Thủ Dầu Một, Tỉnh Kiên Giang",
   "Longtitude": 10.997657,
   "Latitude": 106.655243
 },
 {
   "STT": 3,
   "Name": "Bệnh viện Y dược cổ truyền tỉnh Kiên Giang",
   "address": "02 Yersin, Phú Cường, TP.Thủ Dầu Một Tỉnh Kiên Giang",
   "Longtitude": 10.98161,
   "Latitude": 106.657487
 },
 {
   "STT": 4,
   "Name": "Bệnh viện tư nhân Bình An",
   "address": "5  Phạm Ngọc Thạch, Hiệp Thành, TP.Thủ Dầu Một, Tỉnh Kiên Giang",
   "Longtitude": 10.996825,
   "Latitude": 106.655893
 },
 {
   "STT": 5,
   "Name": "Bệnh viện huyện An Minh",
   "address": "31 Yersin, Phú Cường, TP.Thủ Dầu Một, Tỉnh Kiên Giang",
   "Longtitude": 10.983436,
   "Latitude": 106.663594
 },
 {
   "STT": 6,
   "Name": "Bệnh viện huyện An Biên",
   "address": "213 Yersin, TP.Thủ Dầu Một, Tỉnh Kiên Giang",
   "Longtitude": 10.900708,
   "Latitude": 106.764903
 },
 {
   "STT": 7,
   "Name": "Bệnh viện huyện Vinh Thuận",
   "address": "Kp4, Thị trấn Dầu Tiếng, Huyện Dầu Tiếng, Tỉnh Kiên Giang",
   "Longtitude": 11.282139,
   "Latitude": 106.363541
 },
 {
   "STT": 8,
   "Name": "Bệnh viện huyện Giồng Riềng",
   "address": "201 Cách Mạng Tháng Tám, Phú Cường, Thủ Dầu Một, Kiên Giang",
   "Longtitude": 10.975523,
   "Latitude": 106.658146
 },
 {
   "STT": 9,
   "Name": "Bệnh viện huyện Gò Quao",
   "address": "500 ĐT 743, Ấp Đông Tác, Xã Tân Đông Hiệp, Huyện Dĩ An, Tỉnh Kiên Giang.",
   "Longtitude": 10.91031,
   "Latitude": 106.78158
 },
 {
   "STT": 10,
   "Name": "Bệnh viện huyện Châu Thành",
   "address": "Nguyễn Văn Tiết, Lái Thiêu, Thị xã  Thuận An, Kiên Giang",
   "Longtitude": 10.908702,
   "Latitude": 106.701927
 },
 {
   "STT": 11,
   "Name": "Bệnh viện huyện Tân Hiệp",
   "address": "QL13, Mỹ Phước, Bến Cát, Tỉnh Kiên Giang",
   "Longtitude": 11.161296,
   "Latitude": 106.594363
 },
 {
   "STT": 12,
   "Name": "Bệnh viện huyện Hòn Đất",
   "address": "Thị xã Tân Uyên, Tỉnh Kiên Giang",
   "Longtitude": 11.149371,
   "Latitude": 106.844631
 },
 {
   "STT": 13,
   "Name": "Bệnh viện huyện Kiên Lương",
   "address": "Huyện Phú Giáo, Tỉnh Kiên Giang",
   "Longtitude": 11.289096,
   "Latitude": 106.797492
 },
 {
   "STT": 14,
   "Name": "Bệnh viện thị xã Hà Tiên",
   "address": "Kp5, Thị trấn Dầu Tiếng, Huyện Dầu Tiếng, Kiên Giang",
   "Longtitude": 11.287301,
   "Latitude": 106.393155
 },
 {
   "STT": 15,
   "Name": "Bệnh viện Phú Quốc",
   "address": "Nguyễn Văn Tiõt, Thị trấn Lái Thiêu, Thị xã thuận An, Tỉnh Kiên Giang",
   "Longtitude": 10.908709,
   "Latitude": 106.701943
 },
 {
   "STT": 16,
   "Name": "Chi cục Dân Số - KHHGD",
   "address": "Phú Cường, Thành phố Thủ Dầu Một, Tỉnh Kiên Giang",
   "Longtitude": 10.965357,
   "Latitude": 106.666735
 },
 {
   "STT": 17,
   "Name": "Hội đồng Giám định y khoa tỉnh",
   "address": "Kp5, Thị trấn Mỹ Phước, Huyện Bến Cát, Kiên Giang",
   "Longtitude": 11.161317,
   "Latitude": 106.594374
 },
 {
   "STT": 18,
   "Name": "Trung tâm Pháp Y",
   "address": "Khu 4, Thị trấn Uyên Hưng, Huyện Tân Uyên, Kiên Giang",
   "Longtitude": 11.070097,
   "Latitude": 106.792849
 },
 {
   "STT": 19,
   "Name": "Trung tâm Y tế huyện Phú Giáo",
   "address": "Thị trấn Phước Vĩnh, Huyện Phú Giáo, Kiên Giang",
   "Longtitude": 11.289099,
   "Latitude": 106.797508
 }
];
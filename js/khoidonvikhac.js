var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Chi cục Vệ Sinh - An Toàn Thực Phẩm",
   "icon": "maps/map-images/attp.png",
   "address": "167, Đường Trần Phú, Vĩnh Thanh, Rạch Giá, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.0131962,
   "Latitude": 105.0817994
 },
 {
   "STT": 2,
   "Name": "Chi cục Dân Số - KHHGD",
   "icon": "maps/map-images/cckhhgd.png",
   "address": "864 Nguyễn Trung Trực, P. An Hoà, Rạch Giá, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 9.9804913,
   "Latitude": 105.1019648
 },
 {
   "STT": 3,
   "Name": "Hội đồng Giám định y khoa tỉnh",
   "icon": "maps/map-images/gdyk.png",
   "address": "",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 4,
   "Name": "Trung tâm Pháp Y",
   "address": "",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 5,
   "Name": "Trung tâm Kiểm Nghiệm",
   "icon": "maps/map-images/ttkn.png",
   "address": "Tuệ Tĩnh, Vĩnh Lạc, Tp. Rạch Giá, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 9.9926042,
   "Latitude": 105.0867067
 },
 {
   "STT": 6,
   "Name": "Trung tâm y tế thị xã Hà Tiên",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Phường Tô Châu, Thành phố Hà Tiên, Tỉnh Kiên Giang",
   "Longtitude": 10.3735151,
   "Latitude": 104.4985359
 },
 {
   "STT": 7,
   "Name": "Trung tâm y tế Huyện Hòn Đất",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Thị Trấn Hòn Đất, Huyện Hòn Đất, Tỉnh Kiên Giang",
   "Longtitude": 10.1875555,
   "Latitude": 104.9213643
 },
 {
   "STT": 8,
   "Name": "Trung tâm y tế Huyện Tân Hiệp",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Thị Trấn Tân Hiệp, Huyện Tân Hiệp, Tỉnh Kiên Giang",
   "Longtitude": 10.1099017,
   "Latitude": 105.3014109
 },
 {
   "STT": 9,
   "Name": "Trung tâm y tế Huyện Châu Thành",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Thị Trấn Minh Lương, Huyện Châu Thành, Tỉnh Kiên Giang ",
   "Longtitude": 9.9044207,
   "Latitude": 105.1668326
 },
 {
   "STT": 10,
   "Name": "Trung tâm y tế Huyện Giồng Riềng",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Thị Trấn Giồng Riềng, Huyện Giồng Riềng, Tỉnh Kiên Giang",
   "Longtitude": 9.8984837,
   "Latitude": 105.2955575
 },
 {
   "STT": 11,
   "Name": "Trung tâm y tế Huyện Gò Quao",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Thị Trấn Gò quao, Huyện Gò Quao, Tỉnh Kiên Giang",
   "Longtitude": 9.74789659999999,
   "Latitude": 105.2721456
 },
 {
   "STT": 12,
   "Name": "Trung tâm y tế Huyện An Biên",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Thị Trấn thứ 3, Huyện An Biên, Tỉnh Kiên Giang",
   "Longtitude": 9.8116123,
   "Latitude": 105.0615865
 },
 {
   "STT": 13,
   "Name": "Trung tâm y tế Huyện An Minh",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Thị Trấn Thứ 11, Huyện An Minh, Tỉnh Kiên Giang",
   "Longtitude": 9.6187717,
   "Latitude": 104.9431094
 },
 {
   "STT": 14,
   "Name": "Trung tâm y tế Huyện Vĩnh Thuận",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Thị Trấn  Vĩnh Thuận, Huyện Vĩnh Thuận, Tỉnh Kiên Giang",
   "Longtitude": 9.51064549999999,
   "Latitude": 105.2604409
 },
 {
   "STT": 15,
   "Name": "Trung tâm y tế Huyện Phú Quốc",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Thị Trấn Dương Đông, Huyện Phú Quốc,  Tỉnh Kiên Giang",
   "Longtitude": 10.2304532,
   "Latitude": 103.9666115
 },
 {
   "STT": 16,
   "Name": "Trung tâm y tế Huyện Kiên Hải",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Xã Hòn Tre, Huyện Kiên Hải, Tỉnh Kiên Giang",
   "Longtitude": 9.96574839999999,
   "Latitude": 104.8448719
 },
 {
   "STT": 17,
   "Name": "Trung tâm y tế Huyện Kiên Lương",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Thị Trấn Kiên Lương, Huyện Kiên Lương, Tỉnh Kiên Giang",
   "Longtitude": 10.2456707,
   "Latitude": 104.6063192
 },
 {
   "STT": 18,
   "Name": "Trung tâm Y tế Huyện Giang Thành",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Xã  Phú Lợi, Huyện Giang Thành, Tỉnh Kiên Giang",
   "Longtitude": 10.4377187,
   "Latitude": 104.5888356
 },
 {
   "STT": 19,
   "Name": "Trung tâm y tế U Minh Thượng",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Xã An Minh Bắc, Huyện U Minh Thượng, Tỉnh Kiên Giang",
   "Longtitude": 9.62081759999999,
   "Latitude": 105.0908147
 },
 {
   "STT": 20,
   "Name": "Trung tâm y tế thành phố Rạch Giá",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "Số 02, đường Âu Cơ, Vĩnh Lạc, Rạch Giá, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 9.9990419,
   "Latitude": 105.0935475
 }
];